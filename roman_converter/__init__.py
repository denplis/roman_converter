__version__ = '0.0.1'

ROMAN_SYMBOLS_MAPPING = {
    "M": 1000,
    "CM": 900,
    "D": 500,
    "CD": 400,
    "C": 100,
    "XC": 90,
    "L": 50,
    "XL": 40,
    "X": 10,
    "IX": 9,
    "V": 5,
    "IV": 4,
    "I": 1,
}


def int_to_roman(int_to_convert: int) -> str:
    """
    Conver integer to Roman Numeric
    """
    result = ''

    for r, i in ROMAN_SYMBOLS_MAPPING.items():
        while (int_to_convert // i):
            result += r
            int_to_convert -= i

    return result
