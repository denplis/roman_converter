Installation:
=============

.. code-block:: bash

	pip install git+https://gitlab.com/denplis/roman_converter`


Run tests
=========
.. code-block:: bash

	make test


Usage
=====

Converting Integer to Roman Numeric:

.. code-block:: python

	import roman_converter
	roman_converter.int_to_roman(94)
