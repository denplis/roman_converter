import pytest
import roman_converter


def test_symbols_simple_single():
    assert roman_converter.int_to_roman(111) == 'CXI'


def test_symbols_simple_multiply():
    assert roman_converter.int_to_roman(333) == 'CCCXXXIII'


def test_symbols_with_deduct():
    assert roman_converter.int_to_roman(94) == 'XCIV'


def test_symbols_with_append():
    assert roman_converter.int_to_roman(77) == 'LXXVII'
